|[![The HYIP Project](https://avatars1.githubusercontent.com/u/8466209?v=10&s=20)](https://github.com/hyip) |This [repo](https://github.com/hyip/info "Repository") is courtesy of [The HYIP Project](https://github.com/hyip/monitor "High Yard Investment Program"). Find all of them on [The Project Map](https://github.com/hyip/info/wiki/maps#project-map "Project Mapping").|[![The HYIP Project](https://tophyipmonitor.files.wordpress.com/2015/06/spider.png?w=20)](https://tophyipmonitor.wordpress.com/hyip-monitors/nature-3/#main) |
|:----|----|----:|

# Hyip Monitoring Script
This project builds an integration to run all monitoring code of Hyip Project.   
The script will provide most relevant information about HYIP Monitoring and Rating Services.  

Project chapter are divided in to the following section:

[**HYIP Monitor**][1]  
this section  

[**HYIP Rating**][1]  
<https://github.com/hyip/rating>  

[**HYIP Script**][1]  
<https://github.com/hyip/script>  

Basic information & Background can be found on the following:  

[**HYIP Info**][1]  
<https://github.com/hyip/info>  
  
In this section and deeper will discuss of monitoring code that runnung as a module.    
[Kohana](https://kohanaframework.org/) version 3.0 is being chosen as the php framework for this module.  

Please noted that file residing contain only all files that is modified or made within Kohana Frame Work  
Other files that is not touched will be the same as original sources. 

Using the Kohana request module we develop a hyip script to provide most relevant information about HYIP, High Yield Investment Programs Monitoring and Rating Services.

[Here](https://tophyipmonitor.wordpress.com/2015/05/17/hyip-analysis/) is our page for the first released of this project so far:

[![Version](https://hyip-world.appspot.com/UA-51930984-2/github/monitor?gif=v1.2)](http://all.hyip.world/)  
<a href="http://all.hyip.world/" title="HYIP Score Analysis v1.2" target="_blank">
**HYIP Score Analysis v1.2**</a>  
<http://all.hyip.world/>

A simple table analysis of scoring script to do hyip calculator & sites rating. Help you find legit hyips & monitor your investment against scam. This is a basic core and background philosophy of the project in general.
 
[**HYIP Monitor & HYIP Rating Services**][1]  
Moreover we show you in more details in styles & its best one:    
Hyip Rating-**Style#1**: [*Top 10 Rated HYIPs*](http://tophyips.info/monitor/hyip-rating/style-1), [*The Best Hyip*](http://tophyips.info/monitor/hyip-rating/style-1/best-1) & [*The Latest Scam*](http://tophyips.info/monitor/hyip-scam/style-1) of **US$ 1.00-4.99**  
Hyip Rating-**Style#2**: [*Top 10 Rated HYIPs*](http://tophyips.info/monitor/hyip-rating/style-2), [*The Best Hyip*](http://tophyips.info/monitor/hyip-rating/style-2/best-1) & [*The Latest Scam*](http://tophyips.info/monitor/hyip-scam/style-2) of **US$ 5.00-9.99**  
Hyip Rating-**Style#3**: [*Top 10 Rated HYIPs*](http://tophyips.info/monitor/hyip-rating/style-3), [*The Best Hyip*](http://tophyips.info/monitor/hyip-rating/style-3/best-1) & [*The Latest Scam*](http://tophyips.info/monitor/hyip-scam/style-3) of **US$ 10.00-19.99**  
Hyip Rating-**Style#4**: [*Top 10 Rated HYIPs*](http://tophyips.info/monitor/hyip-rating/style-4), [*The Best Hyip*](http://tophyips.info/monitor/hyip-rating/style-4/best-1) & [*The Latest Scam*](http://tophyips.info/monitor/hyip-scam/style-4) of **US$ 10.00 in BTC**  
Hyip Rating-**Style#5**: [*Top 10 Rated HYIPs*](http://tophyips.info/monitor/hyip-rating/style-5), [*The Best Hyip*](http://tophyips.info/monitor/hyip-rating/style-5/best-1) & [*The Latest Scam*](http://tophyips.info/monitor/hyip-scam/style-5) of **US$ 20-49.99**  
Hyip Rating-**Style#6**: [*Top 10 Rated HYIPs*](http://tophyips.info/monitor/hyip-rating/style-6), [*The Best Hyip*](http://tophyips.info/monitor/hyip-rating/style-6/best-1) & [*The Latest Scam*](http://tophyips.info/monitor/hyip-scam/style-6) of **US$ 50-99.& 99**  
Hyip Rating-**Style#7**: [*Top 10 Rated HYIPs*](http://tophyips.info/monitor/hyip-rating/style-7), [*The Best Hyip*](http://tophyips.info/monitor/hyip-rating/style-7/best-1) & [*The Latest Scam*](http://tophyips.info/monitor/hyip-scam/style-7) of **US$ 100-999.99**  
Hyip Rating-**Style#8**: [*Top 10 Rated HYIPs*](http://tophyips.info/monitor/hyip-rating/style-8), [*The Best Hyip*](http://tophyips.info/monitor/hyip-rating/style-8/best-1) & [*The Latest Scam*](http://tophyips.info/monitor/hyip-scam/style-8) of **US$ 1,000-100K**  


The configuration above is also introduced here:  
Hyip Script on Bitbucket: https://bitbucket.org/hyips  
Youtube Channel : https://www.youtube.com/user/TophyipsInfoNetwork/videos  
Talkgold on Forum Group: http://www.talkgold.com/forum/group.php?discussionid=1004  
Russian Forum: http://mmgp.ru/blog.php?b=2468 & http://www.rusmmg.ru/showthread.php?t=58758

##Update

We are on testing our website to open the world for HYIP Referral Link Submission. Please don't hesitate to take a look and give it a try, for more detail see ['*here*'](http://hyip.world/).

# Rating
This project builds an integration script to run rating code of Hyip Project.

***
|[:house:](https://github.com/hyip) [Home](https://github.com/hyip)|[Next](https://github.com/hyiprating/hyiprating.github.io) [:arrow_forward:](https://github.com/hyiprating/hyiprating.github.io)|
|:----|----:|


  [1]: https://github.com/hyip/info
